package com.example.hw1.repositories;

import com.example.hw1.dto.Book;
import org.apache.log4j.Logger;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;



import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

@Repository
public class BookRepository<T> implements ProjectRepository<Book>, ApplicationContextAware {

    private final Logger logger = Logger.getLogger(BookRepository.class);
    private final List<Book> repo = new LinkedList<>();
    private ApplicationContext context;

    @Override
    public List<Book> retreiveAll() {
        return new LinkedList<>(repo);
    }

    @Override
    public void store(Book book) {
        book.setId(book.hashCode());
        logger.info("store new book: " + book);
        repo.add(book);
    }

    @Override
    public boolean removeItemById(String bookIdToRemove) {
        for (Book book : retreiveAll()) {
            if (book.getId().toString().equals(bookIdToRemove)) {
                logger.info("remove book completed: " + book);
                return repo.remove(book);
            }
        }
        return false;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    private void defaultInit() {
        logger.info("default INIT in book repo bean");
    }

    private void defaultDestroy() {
        logger.info("default DESTROY in book repo bean");
    }
    public void deleteByRegExp(String regAuthor, String regTitle, String regSize) {
        repo.removeIf(book -> Pattern.matches(regAuthor, book.getAuthor())
                && Pattern.matches(regTitle, book.getTitle())
                && Pattern.matches(regSize, book.getSize()));
    }
}


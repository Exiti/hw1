package com.example.hw1.services;

import com.example.hw1.dto.Book;
import com.example.hw1.repositories.BookRepository;
import com.example.hw1.repositories.ProjectRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private final BookRepository<Book> bookRepo;

    @Autowired
    public BookService(BookRepository<Book> bookRepo) {
        this.bookRepo = bookRepo;
    }

    public List<Book> getAllBooks() {
        return bookRepo.retreiveAll();
    }

    public void saveBook(Book book) {
        if (book.getAuthor().length() > 0 || book.getSize().length() > 0
                || book.getTitle().length() > 0 )
        bookRepo.store(book);
    }

    public boolean removeBookById(Integer bookIdToRemove) {
        return bookRepo.removeItemById(bookIdToRemove.toString());
    }
    public void deleteByRegExp(String regAuthor, String regTitle, String regSize) {
        bookRepo.deleteByRegExp(regAuthor,  regTitle,  regSize);
    }
}

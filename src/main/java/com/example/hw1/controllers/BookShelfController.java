package com.example.hw1.controllers;


import com.example.hw1.dto.Book;
import com.example.hw1.services.BookService;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "books")
public class BookShelfController {

    private final Logger logger = Logger.getLogger(BookShelfController.class);
    private final BookService bookService;

    @Autowired
    public BookShelfController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/shelf")
    public String books(Model model) {
        logger.info("got book shelf");
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getAllBooks());
        return "book_shelf";
    }

    @PostMapping("/save")
    public String saveBook(Book book){
        bookService.saveBook(book);
        logger.info("current repository size: "+bookService.getAllBooks().size());
        return "redirect:/books/shelf";
    }

    @DeleteMapping("/remove")
    public String removeBook(@RequestParam("bookIdToRemove") Integer bookIdToRemove){
        bookService.removeBookById(bookIdToRemove);
        return "redirect:/books/shelf";

    }
    @DeleteMapping("/removeByRegex")
    public String removeReg(@RequestParam("regexAuthor") String regAuthor,
                            @RequestParam("regexTitle") String regTitle,
                            @RequestParam("regexSize") String regSize) {
        bookService.deleteByRegExp(regAuthor, regTitle, regSize);
        return "redirect:/books/shelf";
    }
}

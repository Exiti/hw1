package com.example.hw1.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class Book {
    private Integer id;
    private String author;
    private String title;
    private String size;
}
